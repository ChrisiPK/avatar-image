# Avatar Image application for the flow3r badge
# Copyright (C) 2023 ChrisiPK

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from st3m.application import Application, ApplicationContext
import st3m.run
import leds

class AvatarImage(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        ### CONFIGURATION ###

        # Length of the LED trail
        self._trail_length = 10

        # Delay in ms between advancing the LED trail
        self._delay = 100

        # Color for the LED trail
        self._color = (0,255,0)

        ### CONFIGURATION END ###

        self._time_passed = 0
        self._led = 0

    def draw(self, ctx: Context) -> None:
        # Specs say that the display is 240x240 but this resolution leaves some black borders.
        # Assume a resolution of 250x250 instead.
        ctx.image('/flash/sys/apps/avatar-image/avatar.png',-125,-125,250,250)

        for i in range(40):
            distance = (self._led - i) % 40
            if distance < self._trail_length:
                multiplier = 1 - (distance / self._trail_length)
                leds.set_rgb(i,self._color[0] * multiplier,self._color[1] * multiplier,self._color[2] * multiplier)
            else:
                leds.set_rgb(i,0,0,0)
        
        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        self._time_passed += delta_ms
        if self._time_passed >= self._delay:
            self._led += 1
            self._time_passed = 0

# Enable running via mpremote run
if __name__ == '__main__':
    st3m.run.run_view(AvatarImage(ApplicationContext()))
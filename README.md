This application provides a way to use an image as your avatar. Also, it creates a simple LED running light animation.

HOW TO USE:

- Clone the repository and replace avatar.png with your avatar image. Images should be square and will be scaled to 250x250 px.
- Edit __init__.py to change the configuration according to your preferences. 
- Copy the app directory with your image to your badge's flash in /sys/apps/avatar-image
- Restart the badge. You can start the app via the menu Badge->Avatar Image